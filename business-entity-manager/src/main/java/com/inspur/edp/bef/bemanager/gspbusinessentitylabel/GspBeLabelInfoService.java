/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.gspbusinessentitylabel;

import com.inspur.edp.bef.bemanager.gspbusinessentitylabel.repository.GspBeLabelInfoRepository;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.gspbusinessentitylabel.api.IGspBeLabelInfoService;
import com.inspur.edp.bef.bizentity.gspbusinessentitylabel.entity.GspBeLabelInfo;


import java.util.List;

public class GspBeLabelInfoService implements IGspBeLabelInfoService {
	private GspBeLabelInfoRepository labelInfoRepository;

	public GspBeLabelInfoService(GspBeLabelInfoRepository labelInfoRepository) {
		this.labelInfoRepository = labelInfoRepository;
	}

	@Override
	public GspBeLabelInfo getBeLabelInfo(String id) {
		return labelInfoRepository.getOne(id);
	}

	@Override
	public List<GspBeLabelInfo> getBeLabelInfos() {
		return labelInfoRepository.findAll();
	}

	@Override
	public void saveBeLabelInfos(List<GspBeLabelInfo> infos) {
		infos.stream().forEach(item -> {
			CheckInfoUtil.checkNessaceryInfo("id", item.getId());
		});
		labelInfoRepository.saveAll(infos);
	}
}
