/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.cef.designtime.core.utilsgenerator.DataTypeUtilsGenerator;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

public final class BeEntityUtilsGenerator extends DataTypeUtilsGenerator {

  private final GspBizEntityObject bizEntityObject;
  private final String pachageName;

  public BeEntityUtilsGenerator(GspBizEntityObject bizEntityObject, String pachageName,
      String basePath) {
    super(bizEntityObject, basePath);
    this.bizEntityObject = bizEntityObject;
    this.pachageName = pachageName;
  }

  @Override
  protected String getClassName() {
    return bizEntityObject.getCode() + "Utils";
  }

  @Override
  protected String getClassPackage() {
    return pachageName;
  }

  @Override
  protected void generateExtendInfos() {
    super.generateExtendInfos();
    generateGetChildObjectDatas();
  }

  private void generateGetChildObjectDatas() {
    if (bizEntityObject.getContainChildObjects() == null
        || bizEntityObject.getContainChildObjects().size() == 0) {
      return;
    }
    for (IGspCommonObject childObject : bizEntityObject.getContainChildObjects()) {
      new EntityGetChildDatasMethodGenerator(childObject.getCode(), getClassInfo()).generate();
    }
  }
}
