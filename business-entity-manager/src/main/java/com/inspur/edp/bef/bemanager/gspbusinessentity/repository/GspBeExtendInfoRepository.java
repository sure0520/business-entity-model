/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.gspbusinessentity.repository;

import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfoEntity;
import io.iec.edp.caf.data.orm.DataRepository;
import java.util.List;

/**
 * BE扩展信息持久化层服务
 *
 * @author hanll02
 */
public interface GspBeExtendInfoRepository extends DataRepository<GspBeExtendInfoEntity, String> {

  /**
   * 根据指定的BE元数据的configId获取BE扩展信息
   *
   * @param configId BE元数据的configid
   * @return 指定的BE扩展信息
   */
  GspBeExtendInfoEntity getBeExtendInfoByConfigId(String configId);

  /**
   * 根据指定的BE元数据的configid获取与之对应的BE扩展信息
   *
   * @param configId BE元数据的configid
   * @return 指定的BE扩展信息
   */
  List<GspBeExtendInfoEntity> getBeExtendInfosByConfigId(String configId);

}
