/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;

public class JavaB4RetrieveDeterminationGenerator extends JavaDeterminationGenerator {

  public JavaB4RetrieveDeterminationGenerator(GspBusinessEntity be, BizOperation operation,
      String nameSpace, String path) {
    super(be, operation, nameSpace, path);
  }

  @Override
  protected String getBaseClassName() {
    return "AbstractB4RetrieveDetermination";
  }

  @Override
  protected void JavaGenerateConstructor(StringBuilder result) {
    result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(GetCompName()).append("(IBeforeRetrieveDtmContext context)").append(" ").append("{")
        .append(getNewline());
    result.append(GetIndentationStr()).append(GetIndentationStr()).append("super(context)")
        .append(";").append(getNewline()); //添加基类构造函数
    result.append(GetIndentationStr()).append("}");
  }
}
