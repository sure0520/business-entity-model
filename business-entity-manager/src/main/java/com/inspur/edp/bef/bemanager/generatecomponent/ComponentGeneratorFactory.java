/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.generatecomponent;

import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.BaseComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.BeComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.BeMgrComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.DeterminationComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.ValidationComponentGenerator;
import com.inspur.edp.bef.bizentity.beenum.BEOperationType;

public class ComponentGeneratorFactory {

  private static final ComponentGeneratorFactory instance = new ComponentGeneratorFactory();

  public static ComponentGeneratorFactory getInstance() {
    return instance;
  }

  public BaseComponentGenerator getGenerator(BEOperationType opType) {
    switch (opType) {
      case BizAction:
        return BeComponentGenerator.getInstance();
      case Validation:
        return ValidationComponentGenerator.getInstance();
      case BizMgrAction:
        return BeMgrComponentGenerator.getInstance();
      case Determination:
        return DeterminationComponentGenerator.getInstance();
      default:
        throw new RuntimeException("不支持对此动作类型生成构件:" + opType);
    }
  }
}
