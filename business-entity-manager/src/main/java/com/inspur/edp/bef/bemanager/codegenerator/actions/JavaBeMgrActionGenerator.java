/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

public class JavaBeMgrActionGenerator extends JavaBaseActionGenerator {

  ///#region 属性
  private String allInterfaceName;
  private String beMgrInterfaceName;

  @Override
  protected String getBaseClassName() {
    return "AbstractManagerAction<" + ReturnTypeName + ">";
  }

  ///#endregion


  ///#region 构造函数
  public JavaBeMgrActionGenerator(GspBusinessEntity be,
      com.inspur.edp.bef.bizentity.operation.BizOperation operation, String nameSpace,
      String path) {
    super(be, operation, nameSpace, path);
    // todo 依赖
//        allInterfaceName = ApiHelper.getBEAllInterfaceClassName(be.getMainObject());
//        beMgrInterfaceName = ApiHelper.getBEMgrAllInterfaceClassName(be);
    //allInterfaceName = getBEInterfaceClassName(be.getMainObject());
    //beMgrInterfaceName = ApiHelper.getBEMgrInterfaceClassName(be);
  }

  private static String getBEInterfaceClassName(IGspCommonObject obj) {
    return "I" + obj.getCode() + "Entity";
//        String nameSpace = obj.getBelongModel().getApiNamespace().getDefaultNamespace();
//        String ItemNameTemplate = "I" + obj.getCode() + "Entity";
//        return new MediateType(nameSpace, ItemNameTemplate).getClassName();
  }

  @Override
  protected String GetNameSpaceSuffix() {
    return JavaCompCodeNames.MgrActionNameSpaceSuffix;
  }

  ///#endregion

  ///#region GenerateConstructor

  @Override
  protected void JavaGenerateConstructor(StringBuilder result) {
    result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(GetCompName()).append("(");

    //result.append(GetDoubleIndentationStr()).append(GetIndentationStr()).append("IBEManagerContext managerContext");
    result.append("IBEManagerContext managerContext");

    if (JavaHasCustomConstructorParams()) {
      result.append(",");
      JavaGenerateConstructorParams(result);
    } else {
    }

    //result.append(GetDoubleIndentationStr()).append(GetIndentationStr()).append(getNewline()).append(") : base(managerContext)");
    result.append(")");

    result.append(" {").append(getNewline());
    result.append(GetIndentationStr()).append(GetIndentationStr()).append("super(managerContext);")
        .append(getNewline()); //新加项
    JavaGenerateConstructorContent(result);
    result.append(GetIndentationStr()).append("}");
  }

  ///#endregion


  ///#region GenerateExtendMethod
  @Override
  protected void JavaGenerateExtendMethod(StringBuilder result) {
    if (this.isInterpretation) {
      JavaGenerateInterpretationGetEntityMethod(result);
      JavaGenerateInterpretationGetMgrMethod(result);
    } else {
      JavaGenerateGetEntityMethod(result);

      JavaGenerateGetMgrMethod(result);
    }

    //GenerateGetPartialMethod(result);
  }

  private void JavaGenerateGetEntityMethod(StringBuilder result) {
    result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
        .append(allInterfaceName).append(" getEntity(String dataId) ").append("{")
        .append(getNewline());

    //result.append(GetDoubleIndentationStr()).append(GetIndentationStr())
    //  .append("return base.BEManagerContext.GetEntity(dataId) as ").append(allInterfaceName).append(getNewline()).append(" ;");
    result.append(GetIndentationStr()).append(GetIndentationStr()).append("return ").append("(")
        .append(allInterfaceName).append(")")
        .append("super.getBEManagerContext().getEntity(dataId)").append(";").append(getNewline());

    result.append(GetIndentationStr()).append("}").append(getNewline());
  }

  private void JavaGenerateGetMgrMethod(StringBuilder result) {
    result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
        .append(beMgrInterfaceName).append(" getMgr() ").append("{").append(getNewline());

    //result.append(GetDoubleIndentationStr()).append(GetIndentationStr())
    //  .append("return base.BEManagerContext.BEManager as ").append(beMgrInterfaceName).append(getNewline()).append(" ;");
    result.append(GetIndentationStr()).append(GetIndentationStr()).append("return ").append("(")
        .append(beMgrInterfaceName).append(")").append("super.getBEManagerContext().getBEManager()")
        .append(";").append(getNewline());

    result.append(GetIndentationStr()).append("}").append(getNewline());

  }

  private void JavaGenerateInterpretationGetEntityMethod(StringBuilder result) {
    result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
        .append(JavaCompCodeNames.IBusinessEntity).append(" getEntity(String dataId) ").append("{")
        .append(getNewline());

    //result.append(GetDoubleIndentationStr()).append(GetIndentationStr())
    //  .append("return base.BEManagerContext.GetEntity(dataId) as ").append(allInterfaceName).append(getNewline()).append(" ;");
    result.append(GetIndentationStr()).append(GetIndentationStr()).append("return ").append("(")
        .append(JavaCompCodeNames.IBusinessEntity).append(")")
        .append("super.getBEManagerContext().getEntity(dataId)").append(";").append(getNewline());

    result.append(GetIndentationStr()).append("}");
  }

  private void JavaGenerateInterpretationGetMgrMethod(StringBuilder result) {
    result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
        .append(JavaCompCodeNames.IBEService).append(" getMgr() ").append("{").append(getNewline());

    //result.append(GetDoubleIndentationStr()).append(GetIndentationStr())
    //  .append("return base.BEManagerContext.BEManager as ").append(beMgrInterfaceName).append(getNewline()).append(" ;");
    result.append(GetIndentationStr()).append(GetIndentationStr()).append("return ").append("(")
        .append(JavaCompCodeNames.IBEService).append(")")
        .append("super.getBEManagerContext().getBEManager()").append(";").append(getNewline());

    result.append(GetIndentationStr()).append("}");

  }

  ///#endregion

  @Override
  protected String GetInitializeCompName() {
    return BizOperation.getCode() + "MgrAction";
  }
}
