/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bemanager.codegenerator.ParameterInfo;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import sun.management.MethodInfo;

public class EntityGetChildDatasMethodGenerator {

  private final String childNodeCode;
  private final JavaClassInfo classInfo;
  private final MethodInfo methodInfo;

  public EntityGetChildDatasMethodGenerator(String childNodeCode, JavaClassInfo classInfo) {

    this.childNodeCode = childNodeCode;
    this.classInfo = classInfo;
    methodInfo = new MethodInfo();
  }

  /*
   * public static IEntityDataCollection getSOItems(IEntityData data)
   * {
   *    return super.getChildDatas(data,"SOItem");
   * }
   * */
  public void generate() {
    methodInfo.setMethodName("get" + childNodeCode + "s");
    methodInfo.setReturnType(new TypeRefInfo(IEntityDataCollection.class));
    methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
    methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);
    methodInfo.getParameters().add(new ParameterInfo("data", new TypeRefInfo(IEntityData.class)));
    generateMethodBodies();
    classInfo.addMethodInfo(methodInfo);
  }

  private void generateMethodBodies() {
    classInfo.getImportInfos().addImportPackage("com.inspur.edp.cef.entity.entity.EntityDataUtils");
    methodInfo.getMethodBodies()
        .add("return EntityDataUtils.getChildDatas(data,\"" + childNodeCode + "\");");
  }
}
