/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.expression.entity;

import java.util.ArrayList;
import java.util.List;

public class ExpressionSchema {

  private List<ExpressionEntitySet> entitySets;
  private List<ExpressionEntityType> entityTypes;
  private String description;

  public List<ExpressionEntitySet> getEntitySets() {
    if (entitySets == null) {
      entitySets = new ArrayList<>();
    }
    return entitySets;
  }

  public void setEntitySets(
      List<ExpressionEntitySet> entitySets) {
    this.entitySets = entitySets;
  }

  public List<ExpressionEntityType> getEntityTypes() {
    if (entityTypes == null) {
      entityTypes = new ArrayList<>();
    }
    return entityTypes;
  }

  public void setEntityTypes(
      List<ExpressionEntityType> entityTypes) {
    this.entityTypes = entityTypes;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
