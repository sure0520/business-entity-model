/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.determination.DeterminationComponent;

public class DeterminationComponentGenerator extends BaseComponentGenerator {

  public static DeterminationComponentGenerator getInstance() {
    return new DeterminationComponentGenerator();
  }

  private DeterminationComponentGenerator() {
  }

  @Override
  protected GspComponent buildComponent() {
    return new DeterminationComponent();
  }


  ///#region 将Determination中信息赋值给DeterminationComponent
  @Override
  protected void evaluateComponentInfo(GspComponent component, BizOperation determination,
      GspComponent originalComponent) {
    this.originalComponent = originalComponent;
    // 基本信息（没有参数信息）
    evaluateComponentBasicInfo((DeterminationComponent) component, (Determination) determination);
    // 返回值信息
    ((DeterminationComponent) component).getDeterminationMethod()
        .setReturnValue(new VoidReturnType());

  }

  private void evaluateComponentBasicInfo(DeterminationComponent component,
      Determination determination) {
    if (!CheckInfoUtil.checkNull(determination.getComponentId())) {
      component.setComponentID(determination.getComponentId());
    }
    component.setComponentCode(determination.getCode());
    component.setComponentName(determination.getName());
    component.setComponentDescription(determination.getDescription());
    component.getDeterminationMethod().setDotnetAssembly(this.assemblyName);

    String suffix = String.format("%1$s%2$s%3$s", determination.getOwner().getCode(), '.',
        JavaCompCodeNames.DeterminationNameSpaceSuffix);
    String packageName = JavaModuleImportPackage(this.nameSpace);
    packageName = String.format("%1$s%2$s", packageName, suffix.toLowerCase());

    if (this.originalComponent != null) {
      component.getDeterminationMethod()
          .setDotnetClassName(this.originalComponent.getMethod().getDotnetClassName());
      component.getMethod().setClassName(
          JavaModuleClassName(this.originalComponent.getMethod().getDotnetClassName(),
              packageName));
    } else {
      String classNameSuffix = this.objCode + determination.getCode();

      component.getDeterminationMethod().setDotnetClassName(ComponentClassNameGenerator
          .generateDeterminationComponentClassName(this.nameSpace, classNameSuffix));
      component.getMethod().setClassName(JavaModuleClassName(ComponentClassNameGenerator
          .generateDeterminationComponentClassName(this.nameSpace, classNameSuffix), packageName));
    }


  }

  ///#endregion
}
