/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.util.generate;

import com.inspur.edp.cef.designtime.api.element.GspElementDataType;

public final class ElementGeneratingUtils {
	@SuppressWarnings("rawtypes")
	public static Class getNativeType(GspElementDataType elementType) {
		switch (elementType) {
		case String:
		case Text:
			return String.class;
		case Boolean:
			return Boolean.class;
		case Integer:
			return Integer.class;
		case Decimal:
			return java.math.BigDecimal.class;
		case Date:
		case DateTime:
			return java.util.Date.class;
		case Binary:
			return byte[].class;
		default:
			// throw new Exception(String.format("elementType'%1$s'invalid enum values"),
			// elementType.toString());
			return null;
		}
	}

}
