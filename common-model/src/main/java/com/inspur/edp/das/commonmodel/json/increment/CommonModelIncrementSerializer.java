/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import lombok.var;

import java.io.IOException;
import java.util.HashMap;

public abstract class CommonModelIncrementSerializer extends JsonSerializer<CommonModelIncrement> {

    @Override
    public void serialize(CommonModelIncrement value, JsonGenerator gen, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        writeExtendInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(CommonModelIncrement value, JsonGenerator gen) {
        //①MainObject
        if(value.getMainEntityIncrement() != null){
            SerializerUtils.writePropertyName(gen, CommonModelNames.MainObject);
            CommonObjectIncrementSerializer objSerializer = getObjectIncrementSerializer();

            try {
                objSerializer.serialize(value.getMainEntityIncrement(), gen, null);
            } catch (IOException e) {
                throw new RuntimeException("增量结构主实体序列化失败", e);
            }
        }

        //②prop
        HashMap<String, PropertyIncrement> properties = value.getChangeProperties();
        if (properties != null && properties.size() > 0)
            writeProperties(properties, gen);
    }

    protected abstract CommonObjectIncrementSerializer getObjectIncrementSerializer();

    private void writeProperties(HashMap<String, PropertyIncrement> properties, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.PropertyIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : properties.entrySet()) {
            SerializerUtils.writeStartObject(gen);
            SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

            SerializerUtils.writePropertyName(gen, CefNames.Value);
            PropertyIncrementSerializer serializer = getPropertyIncrementSerializer();
            serializer.serialize(item.getValue(), gen, null);
            SerializerUtils.writeEndObject(gen);

        }
        SerializerUtils.WriteEndArray(gen);
    }

    private PropertyIncrementSerializer getPropertyIncrementSerializer() {
        return new PropertyIncrementSerializer();
    }

    protected void writeExtendInfo(CommonModelIncrement value, JsonGenerator gen) {

    }
}
