/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.entity.element.increment.merger;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonFieldControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldContrulRuleDef;
import com.inspur.edp.cef.designtime.api.element.increment.ModifyFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.merger.CommonFieldIncrementMerger;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.increment.merger.MergeUtils;
import com.inspur.edp.cef.designtime.api.increment.property.BooleanPropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.StringPropertyIncrement;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

import java.util.HashMap;

public class CommonElementMerger extends CommonFieldIncrementMerger {

    public CommonElementMerger() {
        super();
    }

    public CommonElementMerger(boolean includeAll) {
        super(includeAll);
    }

    @Override
    protected void mergeExtendField(GspCommonField extendField, ModifyFieldIncrement extendIncrement, ModifyFieldIncrement baseIncrement, CommonFieldControlRule rule, CommonFieldContrulRuleDef def) {

    }

    protected final void dealExtendChangeProp(GspCommonField extendField, HashMap<String, PropertyIncrement> extendIncrement, String key, PropertyIncrement increment) {

        GspCommonElement element = (GspCommonElement) extendField;
        switch (key) {
            case CommonModelNames.ColumnID:
                String mergedColumnID = MergeUtils.getStringValue(CommonModelNames.ColumnID, (StringPropertyIncrement) increment, extendIncrement, null, null);
                element.setColumnID(mergedColumnID);
                return;
            case CommonModelNames.BelongModelID:
                String mergedBelongModelID = MergeUtils.getStringValue(CommonModelNames.BelongModelID, (StringPropertyIncrement) increment, extendIncrement, null, null);
                element.setBelongModelID(mergedBelongModelID);
                return;
            case CommonModelNames.Readonly:
                boolean mergedReadonly = MergeUtils.getBooleanValue(CommonModelNames.Readonly, (BooleanPropertyIncrement) increment, extendIncrement, null, null);
                element.setReadonly(mergedReadonly);
                return;
            case CommonModelNames.IsCustomItem:
                boolean mergedIsCustomItem = MergeUtils.getBooleanValue(CommonModelNames.IsCustomItem, (BooleanPropertyIncrement) increment, extendIncrement, null, null);
                element.setIsCustomItem(mergedIsCustomItem);
                return;
        }

        dealCommonElementChangeProp(element, extendIncrement, key, increment);
    }

    protected void dealCommonElementChangeProp(GspCommonElement element, HashMap<String, PropertyIncrement> extendIncrement, String key, PropertyIncrement increment) {

    }
}
