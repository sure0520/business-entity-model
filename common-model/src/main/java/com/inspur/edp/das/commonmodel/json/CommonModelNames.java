/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json;

/**
 * The Serializer Names Of  Common Model
 *
 * @ClassName: CmObjectSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonModelNames
{
	public static final String Model = "Model";
	public static final String ExtendList = "ExtendList";
	public static final String ExtendModel = "ExtendModel";
	public static final String MainObject = "MainObject";
	public static final String Variables = "Variables";
	public static final String Variables_Json = "Variables_Json";
	public static final String ChildObjectList = "ChildObjectList";
	public static final String ElementList = "ElementList";
	public static final String Element = "Element";
	public static final String ChildObject = "ChildObject";
	public static final String BeLabel="BeLabel";
	public static final String IsSimplifyGen ="IsSimplifyGen";
	public static final String IsSimpBeanConfig ="IsSimpBeanConfig";

	/** 
	 分级信息
	 
	*/
	public static final String HirarchyInfo= "HirarchyInfo";

	/** 
	 分级字段
	 
	*/
	public static final String PathElement = "PathElement";

	/** 
	 
	 
	*/
	public static final String PathLength= "PathLength";

	/** 
	 
	 
	*/
	public static final String ParentElement = "ParentElement";

	public static final String ParentRefElement ="ParentRefElement";

	public static final String PathGenerateType = "PathGenerateType";

	/** 
	 sListCode
	 
	*/
	public static final String LayerElement="LayerElement";

	/** 
	 sListCode
	 
	*/
	public static final String ISDETAILELEMENT= "IsDetailElement";
	public static final String ID = "ID";
	public static final String DESCRIPTION = "Description";
	public static final String ColumnGenerateID = "ColumnGenerateID";
	public static final String ElementID = "ElementID";
	public static final String GenerateType = "GenerateType";
	public static final String ColumnParameters = "ColumnParameters";
	public static final String GernerateType = "GernerateType";
	public static final String Code = "Code";
	public static final String Flag = "Flag";
	public static final String Name = "Name";
	public static final String IsRecord = "IsRecord";
	public static final String RecordHistoryTable = "RecordHistoryTable";
	public static final String LogObjectID = "LogObjectID";
	public static final String IsVirtual = "IsVirtual";
	public static final String EntityType = "EntityType";
	public static final String RefObjectName = "RefObjectName";
	public static final String ObjectType = "ObjectType";
	public static final String LogicDelete = "LogicDelete";
	public static final String COLUMN = "COLUMN";
	public static final String GENERATETYPE = "GENERATETYPE";
	public static final String OrderbyCondition = "OrderbyCondition";
	public static final String FilterCondition = "FilterCondition";
	public static final String ModifiedDateElementID = "ModifiedDateElementID";
	public static final String CreatorElementID = "CreatorElementID";
	public static final String CreatedDateElementID = "CreatedDateElementID";
	public static final String ModifierElementID = "ModifierElementID";
	public static final String RecordDelData = "RecordDelData";
	public static final String IsReadOnly = "IsReadOnly";
	public static final String Constraints = "Constraints";
	public static final String Constraint = "Constraint";
	public static final String ConstraintType = "ConstraintType";
	public static final String ConstraintMessage = "ConstraintMessage";
	public static final String ConstraintColumns = "ConstraintColumns";
	public static final String ConstraintColumn = "ConstraintColumn";
	public static final String MDataType = "MDataType";

	public static final String LabelID = "LabelID";
	public static final String CanBillCode = "CanBillCode";
	public static final String BillCodeID = "BillCodeID";
	public static final String BillCodeName = "BillCodeName";
	public static final String CodeGenerateType = "CodeGenerateType";
	public static final String CodeGenerateOccasion = "CodeGenerateOccasion";
	public static final String DefaultValue = "DefaultValue";
	public static final String DefaultValueType = "DefaultValueType";
	public static final String Length = "Length";
	public static final String Precision = "Precision";
	public static final String ColumnID = "ColumnID";
	public static final String CustomExpression = "CustomExpression";
	public static final String IsRequire = "IsRequire";
	public static final String Readonly = "Readonly";
	public static final String IsRefElement = "IsRefElement";
	public static final String RefElementID = "RefElementID";
	public static final String IsMultiLanguage = "IsMultiLanguage";
	public static final String ChildAssociations = "ChildAssociations";

	public static final String Association = "Association";
	public static final String IsCustomItem = "IsCustomItem";
	public static final String RefModelID = "RefModelID";
	public static final String RefModelCode = "RefModelCode";
	public static final String RefModelName = "RefModelName";
	public static final String RefModelPkgName = "RefModelPkgName";
	public static final String Asso_RefObjectID = "RefObjectID";
	public static final String Asso_RefObjectCode = "RefObjectCode";
	public static final String Asso_RefObjectName = "RefObjectName";
	public static final String KeyCollection = "KeyCollection";
	public static final String AssociationKey = "AssociationKey";
	public static final String RefElementCollection = "RefElementCollection";
	public static final String Where = "Where";
	public static final String AssSendMessage = "AssSendMessage";
	public static final String ForeignKeyConstraintType = "ForeignKeyConstraintType";
	public static final String DeleteRuleType = "DeleteRuleType";
	public static final String JoinMode = "JoinMode";
	public static final String SourceElement = "SourceElement";
	public static final String SourceElementDisplay = "SourceElementDisplay";
	public static final String TargetElement = "TargetElement";
	public static final String TargetElementDisplay = "TargetElementDisplay";
	public static final String EnumValues = "EnumValues";
	public static final String EnumValue = "EnumValue";
	public static final String Value = "Value";
	public static final String IsAllowDerive = "IsAllowDerive";
	public static final String IsAllowExtend = "IsAllowExtend";
	public static final String Dimension = "Dimension";
	public static final String Hierarchy = "Hierarchy";
	public static final String StateElementID = "StateElementID";
	public static final String BelongModelID = "BelongModelID";
	public static final String IsRef = "IsRef";
	public static final String IsFromAssoUdt = "IsFromAssoUdt";
	public static final String EnableDynamicProp = "EnableDynProp";
	public static final String RepositoryComps = "RepositoryComps";
	// 字段集合类型
	public static final String CollectionType = "CollectionType";
	/** 
	 外键关联集合
	 
	*/
	public static final String FkConstraints= "FkConstraints";

	/** 
	 外键关联
	 
	*/
	public static final String FkConstraint= "FkConstraint";

	//为json序列化新增
	public static final String ContainEnumValues = "ContainEnumValues";
	public static final String BillCodeConfig = "BillCodeConfig";
	public static final String ContainElements = "ContainElements";
	public static final String ContainChildObjects = "ContainChildObjects";
	public static final String PrimayKeyID = "PrimayKeyID";
	public static final String ExtendNodeList = "ExtendNodeList";
	public static final String ExtProperties = "ExtProperties";
	public static final String Resource = "Resource";
	public static final String ShowMessage = "ShowMessage";
	public static final String Type = "Type";
	public static final String TableName = "TableName";
	public static final String ColumnName = "ColumnName";
	public static final String ContainConstraints = "ContainConstraints";
	public static final String Keys = "Keys";
	public static final String GeneratingAssembly = "GeneratingAssembly";
	public static final String IsUseNamespaceConfig = "IsUseNamespaceConfig";
	// Udt相关
	public static final String IsUdt = "IsUdt";
	public static final String UdtPkgName = "UdtPkgName";
	public static final String UdtID = "UdtID";
	public static final String UdtName = "UdtName";
	public static final String UnifiedDataType = "UnifiedDataType";

	public static final String MappingRelation = "MappingRelation";
	public static final String MappingInfo = "MappingInfo";
	public static final String ChildElements = "ChildElements";
	public static final String KeyInfo = "KeyInfo";
	public static final String ValueInfo = "ValueInfo";
	public static final String Index = "Index";
	public static final String IsDefaultEnum = "IsDefaultEnum";

	// 变量
	public static final String VariableSourceType = "VariableSourceType";
	// 版本字段
	public static final String VersionControlInfo = "VersionControlInfo";
	public static final String VersionControlElementId = "VersionControlElementId";

}
