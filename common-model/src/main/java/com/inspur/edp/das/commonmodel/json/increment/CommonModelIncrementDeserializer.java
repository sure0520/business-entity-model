/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.increment.ModifyFieldIncrement;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

import java.io.IOException;

public abstract class CommonModelIncrementDeserializer extends JsonDeserializer<CommonModelIncrement> {
    @Override
    public CommonModelIncrement deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        CommonModelIncrement modelIncrement = createCommonModelIncrement();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(jsonParser);

        readBaseInfo(modelIncrement, node);
        readExtendInfo(modelIncrement, node);

        return modelIncrement;
    }
    protected CommonModelIncrement createCommonModelIncrement(){
        return new CommonModelIncrement();
    }

    private void readBaseInfo(CommonModelIncrement value, JsonNode node){
        JsonNode objNode = node.get(CommonModelNames.MainObject);
        if(objNode != null)
            readMainObjIncrement(value, objNode);

        JsonNode propIncrements = node.get(CefNames.PropertyIncrements);
        if (propIncrements != null)
            readPropertyIncrementsInfo(value, propIncrements);
    }

    //region MainObject
    private void readMainObjIncrement(CommonModelIncrement value, JsonNode node){
        CommonObjectIncrementDeserializer deserializer = getObjectIncrementDeserializer();
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(CommonEntityIncrement.class, deserializer);
        mapper.registerModule(module);
        try {
            value.setMainEntityIncrement((ModifyEntityIncrement) mapper.readValue(mapper.writeValueAsString(node), CommonEntityIncrement.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException("IGspCommonField反序列化失败", e);
        } catch (IOException e) {
            throw new RuntimeException("IGspCommonField反序列化失败", e);
        }
    }

    protected abstract CommonObjectIncrementDeserializer getObjectIncrementDeserializer();

    //endregion

    //region props

    private void readPropertyIncrementsInfo(CommonModelIncrement increment, JsonNode node) {
        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array) {
            PropertyIncrementDeserializer serializer = getPropertyIncrementSerializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(PropertyIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(CefNames.Value);

                PropertyIncrement prop = mapper.readValue(mapper.writeValueAsString(valueNode), PropertyIncrement.class);
                increment.getChangeProperties().put(key, prop);
            } catch (JsonProcessingException e) {
                throw new RuntimeException("PropertyIncrement反序列化失败", e);
            } catch (IOException e) {
                throw new RuntimeException("PropertyIncrement反序列化失败", e);
            }
        }
    }
    private PropertyIncrementDeserializer getPropertyIncrementSerializer() {
        return new PropertyIncrementDeserializer();
    }
    //endregion

    protected ArrayNode trans2Array(JsonNode node) {
        try {
            return new ObjectMapper().treeToValue(node, ArrayNode.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("JsonNode转化失败，检查Json结构", e);
        }
    }

    protected void readExtendInfo(CommonModelIncrement value, JsonNode node){

    }
}
