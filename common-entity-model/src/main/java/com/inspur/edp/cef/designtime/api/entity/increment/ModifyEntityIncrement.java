/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.entity.increment;

import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.ModifyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import java.util.HashMap;

public class ModifyEntityIncrement extends CommonEntityIncrement implements ModifyIncrement {
    private HashMap<String, PropertyIncrement> changeProperties;
    private HashMap<String, GspCommonFieldIncrement> fields = new HashMap<>();
    private HashMap<String, CommonEntityIncrement> childEntitis = new HashMap<>();

    public HashMap<String, PropertyIncrement> getChangeProperties() {
        if (changeProperties == null)
            changeProperties = new HashMap<>();
        return changeProperties;
    }

    public HashMap<String, GspCommonFieldIncrement> getFields() {
        return fields;
    }

    public HashMap<String, CommonEntityIncrement> getChildEntitis() {
        return childEntitis;
    }

    @Override
    public final IncrementType getIncrementType() {
        return IncrementType.Modify;
    }
}
