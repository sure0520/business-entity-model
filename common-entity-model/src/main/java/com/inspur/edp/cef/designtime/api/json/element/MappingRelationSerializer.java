/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.entity.MappingRelation;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.io.IOException;

public class MappingRelationSerializer extends JsonSerializer<MappingRelation> {
    @Override
    public void serialize(
        MappingRelation mapping, JsonGenerator writer, SerializerProvider serializers) throws IOException {
        if (mapping == null || mapping.getCount() <= 0)
            return;
        SerializerUtils.WriteStartArray(writer);
        for(String key : mapping.getKeys())
        {
            SerializerUtils.writeStartObject(writer);
            SerializerUtils.writePropertyValue(writer, key, mapping.getMappingInfo(key));
            SerializerUtils.writeEndObject(writer);
        }
        SerializerUtils.WriteEndArray(writer);
    }
}
