/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.increment.merger;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.increment.property.BooleanPropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.IntPropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.ObjectPropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.StringPropertyIncrement;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import java.util.HashMap;

public class MergeUtils {

    public static int getIntValueAllowShort(
            String propName,
            IntPropertyIncrement baseIncrement,
            HashMap<String, PropertyIncrement> extendIncrement,
            ControlRuleItem rule,
            ControlRuleDefItem ruleDefItem) {

        int baseValue = (int)baseIncrement.getPropertyValue();
        int extendValue = 0;
        boolean hasExtend = false;
        if(extendIncrement != null && extendIncrement.containsKey(propName)){
            extendValue = (int)((IntPropertyIncrement)extendIncrement.get(propName)).getPropertyValue();
            hasExtend = true;
        }

        if(extendValue > baseValue)
            throw new CAFRuntimeException("", "",
                    "属性"+propName+"的不允许增加", null, ExceptionLevel.Error);
        return mergeValue(baseValue, extendValue, propName, rule, ruleDefItem, hasExtend);
    }

    public static int getIntValue(
            String propName,
            IntPropertyIncrement baseIncrement,
            HashMap<String, PropertyIncrement> extendIncrement,
            ControlRuleItem rule,
            ControlRuleDefItem ruleDefItem){
        int baseValue = (int)baseIncrement.getPropertyValue();
        int extendValue = 0;
        boolean hasExtend = false;
        if(extendIncrement != null && extendIncrement.containsKey(propName)){
            extendValue = (int)((IntPropertyIncrement)extendIncrement.get(propName)).getPropertyValue();
            hasExtend = true;
        }

        return mergeValue(baseValue, extendValue, propName, rule, ruleDefItem, hasExtend);
    }

    public static String getStringValue(
            String propName,
            StringPropertyIncrement baseIncrement,
            HashMap<String, PropertyIncrement> extendIncrement,
            ControlRuleItem rule,
            ControlRuleDefItem ruleDefItem){
        String baseName = baseIncrement.getPropertyValue().toString();
        String extendName = null;
        boolean hasExtend = false;
        if(extendIncrement != null && extendIncrement.containsKey(propName)){
            extendName = ((StringPropertyIncrement)extendIncrement.get(propName)).getPropertyValue().toString();
            hasExtend = true;
        }

        return mergeValue(baseName, extendName, propName, rule, ruleDefItem, hasExtend);
    }

    public static boolean getBooleanValue(
            String propName,
            BooleanPropertyIncrement baseIncrement,
            HashMap<String, PropertyIncrement> extendIncrement,
            ControlRuleItem rule,
            ControlRuleDefItem ruleDefItem){
        boolean baseValue = (boolean)baseIncrement.getPropertyValue();
        Boolean extendValue = null;
        boolean hasExtend = false;
        if(extendIncrement != null && extendIncrement.containsKey(propName)){
            extendValue = (Boolean)((BooleanPropertyIncrement)extendIncrement.get(propName)).getPropertyValue();
            hasExtend = true;
        }

        return mergeValue(baseValue, extendValue, propName, rule, ruleDefItem, hasExtend);
    }

    public static <T> T getObjectValue(
            String propName,
            ObjectPropertyIncrement baseIncrement,
            HashMap<String, PropertyIncrement> extendIncrement,
            ControlRuleItem rule,
            ControlRuleDefItem ruleDefItem){
        T baseValue = (T)baseIncrement.getPropertyValue();
        T extendValue = null;
        boolean hasExtend = false;
        if(extendIncrement != null && extendIncrement.containsKey(propName)){
            extendValue = (T)((ObjectPropertyIncrement)extendIncrement.get(propName)).getPropertyValue();
            hasExtend = true;
        }

        return mergeValue(baseValue, extendValue, propName, rule, ruleDefItem, hasExtend);
    }

    public static String mergeValue(
            String baseValue,
            String extendValue,
            String propName,
            ControlRuleItem rule,
            ControlRuleDefItem ruleDefItem,
            boolean hasExtend) {

        if (!isAllow(propName, baseValue, extendValue, rule, ruleDefItem))
            return baseValue;
        if (hasExtend)
            return extendValue;
        return baseValue;
    }

    public static int mergeValue(
            int baseValue,
            int extendValue,
            String propName,
            ControlRuleItem rule,
            ControlRuleDefItem ruleDefItem,
            boolean hasExtend) {

        if (!isAllow(propName, baseValue, extendValue, rule, ruleDefItem))
            return baseValue;
        if (hasExtend)
            return extendValue;
        return baseValue;
    }

    public static <T> T  mergeValue(
            T baseValue,
            T extendValue,
            String propName,
            ControlRuleItem rule,
            ControlRuleDefItem ruleDefItem,
            boolean hasExtend) {

        if (!isAllow(propName, baseValue, extendValue, rule, ruleDefItem))
            return baseValue;
        if (hasExtend)
            return extendValue;
        return baseValue;
    }

    private static boolean isAllow(
            String propertyName, Object oldValue, Object newValue, ControlRuleItem rule, ControlRuleDefItem ruleDefItem) {
        if(rule == null && ruleDefItem == null)
            return true;
        switch (rule.getControlRuleValue()) {
            case Allow:
                return true;
            case Forbiddon:
                return false;
            case Default:
                return ruleDefItem.getDefaultRuleValue() == ControlRuleValue.Allow;
            default:
                throw new RuntimeException("没有找到对应的控制方式:"+rule.getControlRuleValue());
        }
    }
  public static String getKeyPrefix(String currentKey, String preFix){
    return String.format("%1$s.%2$s",currentKey,preFix);
  }
}
