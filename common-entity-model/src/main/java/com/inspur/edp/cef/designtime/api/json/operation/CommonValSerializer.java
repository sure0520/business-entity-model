/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildValTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonTriggerPointType;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The Json Serializer Of Common Validation
 *
 * @ClassName: CommonValSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonValSerializer extends CommonOpSerializer {
	@Override
	protected void writeExtendCommonOpBaseProperty(JsonGenerator writer, CommonOperation info) {

	}
    public CommonValSerializer(){}
	public CommonValSerializer(boolean full){
		super(full);
		isFull = full;
	}
	@Override
	protected void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info) {
		CommonValidation val = (CommonValidation) info;
		if(isFull||(!val.getGetExecutingDataStatus().isEmpty()&&!val.getGetExecutingDataStatus().iterator().next().equals(ExecutingDataStatus.None))){
			SerializerUtils.writePropertyName(writer, CefNames.GetExecutingDataStatus);
			writeGetExecutingDataStatus(writer, val.getGetExecutingDataStatus());
		}
		if(isFull||(val.getTriggerPointType() != CommonTriggerPointType.None)){
			SerializerUtils.writePropertyValue(writer, CefNames.TriggerPointType, val.getTriggerPointType().getValue());
		}
		writeRequestElements(writer, val.getRequestElements());
		if((isFull && val.getChildTriggerInfo() !=null) || isSerialChildTriggerInfo(val.getChildTriggerInfo()))
			writeChildTriggerInfo(writer, val.getChildTriggerInfo());
//		writeRequestChildElements(writer, val);
		writeRequestChildElements(writer, val);
	}


	private void writeRequestElements(JsonGenerator writer, ValElementCollection childElementsIds) {
		if (childElementsIds == null || childElementsIds.size() <= 0)
			return;
		SerializerUtils.writePropertyName(writer, CefNames.RequestElements);
		SerializerUtils.WriteStartArray(writer);

		for (String childElementsId : childElementsIds)
			if(isFull||(childElementsId!=null&&!"".equals(childElementsId)))
				SerializerUtils.writePropertyValue_String(writer, childElementsId);

		SerializerUtils.WriteEndArray(writer);
	}

	private boolean isSerialChildTriggerInfo(HashMap<String, ChildValTriggerInfo> hashMap){
		if(hashMap == null || hashMap.size() == 0)
			return false;
		return true;
	}

	protected void writeChildTriggerInfo(JsonGenerator writer, HashMap<String, ChildValTriggerInfo> hashMap) {
//		SerializerUtils.writePropertyName(writer, CefNames.ChildTriggerInfo);
//		SerializerUtils.writeStartObject(writer);
//		if(isFull||(!childTriggerInfo.getGetExecutingDataStatus().isEmpty()&&!childTriggerInfo.getGetExecutingDataStatus().iterator().next().equals(ExecutingDataStatus.None))){
//			SerializerUtils.writePropertyName(writer, CefNames.GetExecutingDataStatus);
//			writeGetExecutingDataStatus(writer, childTriggerInfo.getGetExecutingDataStatus());
//		}
//		if(isFull||(childTriggerInfo.getRequestChildElements()!=null&&childTriggerInfo.getRequestChildElements().size()>0))
//			writeRequestChildElements(writer, childTriggerInfo.getRequestChildElements());
//		SerializerUtils.writeEndObject(writer);
		SerializerUtils.writePropertyName(writer, CefNames.ChildTriggerInfo);
		SerializerUtils.WriteStartArray(writer);
		if(hashMap != null && hashMap.size() > 0){
			for(Map.Entry<String, ChildValTriggerInfo> item: hashMap.entrySet()){
				SerializerUtils.writeStartObject(writer);
				SerializerUtils.writePropertyValue(writer, CefNames.RequestChildCode, item.getKey());
				SerializerUtils.writePropertyName(writer, CefNames.RequestDtmChildElementValue);
				ChildValTriggerInfoSerializer valTriggerInfoSerializer = new ChildValTriggerInfoSerializer(isFull);
				try {
					valTriggerInfoSerializer.serialize(item.getValue(), writer, null);
				} catch (IOException e) {
					e.printStackTrace();
				}
				SerializerUtils.writeEndObject(writer);
			}
		}
		SerializerUtils.WriteEndArray(writer);
	}
	private void writeValidationElementCollection(JsonGenerator writer, ValElementCollection childElementsIds) {
		//[
		SerializerUtils.WriteStartArray(writer);
		if (childElementsIds.size() > 0) {
			for (String item : childElementsIds) {
				SerializerUtils.writePropertyValue_String(writer, item);
			}
		}
		//]
		SerializerUtils.WriteEndArray(writer);
	}


	private void writeRequestChildElements(JsonGenerator writer, CommonValidation dtm) {
		HashMap<String, ValElementCollection> dic = dtm.getRequestChildElements();
		if (!isFull && (dic == null || dic.size() <= 0)) {
			return;
		}
		SerializerUtils.writePropertyName(writer, CefNames.RequestChildElements);
		SerializerUtils.WriteStartArray(writer);
		for (Map.Entry<String, ValElementCollection> item : dic.entrySet())
		{
			SerializerUtils.writeStartObject(writer);
			SerializerUtils.writePropertyValue(writer, CefNames.RequestChildElementKey, item.getKey());
			SerializerUtils.writePropertyName(writer, CefNames.RequestChildElementValue);
			writeValidationElementCollection(writer, item.getValue());
			SerializerUtils.writeEndObject(writer);
		}
		SerializerUtils.WriteEndArray(writer);
	}
}
