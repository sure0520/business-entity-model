/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.i18n.names;

/**
 * 国际化资源中午描述
 *
 * @author haoxiaofei
 */
public final class CefResourceDescriptionNames {
    /**
     * 名称
     */
    public static final String Name = "名称";
    /**
     * 字段枚举
     */
    public static final String DisplayValue = "枚举显示值";
}
