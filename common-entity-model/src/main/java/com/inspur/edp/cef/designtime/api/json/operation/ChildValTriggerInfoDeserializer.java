/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildValTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;
import java.util.EnumSet;

public class ChildValTriggerInfoDeserializer extends JsonDeserializer<ChildValTriggerInfo> {
    @Override
    public ChildValTriggerInfo deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartObject(jsonParser);
        ChildValTriggerInfo childTriggerInfo = new ChildValTriggerInfo();
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propertyName = SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName) {
                case CefNames.GetExecutingDataStatus:
                    childTriggerInfo.setGetExecutingDataStatus(readGetExecutingDataStatus(jsonParser));
                    break;
                case CefNames.RequestChildElements:
                    childTriggerInfo.setRequestChildElements(readRequestChildElements(jsonParser));
                    break;
                default:
                    throw new RuntimeException(String.format("找不到属性%1$s", propertyName));
            }
        }
        SerializerUtils.readEndObject(jsonParser);
        return childTriggerInfo;
    }

    protected EnumSet<ExecutingDataStatus> readGetExecutingDataStatus(JsonParser jsonParser) {
        EnumSet<ExecutingDataStatus> result = EnumSet.noneOf(ExecutingDataStatus.class);
        int intValueSum = SerializerUtils.readPropertyValue_Integer(jsonParser);
        ExecutingDataStatus[] values = ExecutingDataStatus.values();
        for (int i = values.length - 1; i >= 0; i--) {
            ExecutingDataStatus value = values[i];
            if (intValueSum > 0 && intValueSum >= value.getValue()) {
                result.add(value);
                intValueSum -= value.getValue();
            }
        }
        return result;
    }

    private ValElementCollection readRequestChildElements(JsonParser jsonParser) {
        RequestChildElementsDeserializer deserializer = new ValRequestChildElementsDeserializer();
        ValElementCollection collection = new ValElementCollection();
        SerializerUtils.readArray(jsonParser, new StringDeserializer(), collection, true);
        return collection;
    }
}
