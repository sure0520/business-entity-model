/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.element.increment.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.element.increment.AddedFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.DeletedFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.ModifyFieldIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldSerializer;
import java.io.IOException;
import java.util.HashMap;
import lombok.var;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class GspFieldIncrementSerializer extends JsonSerializer<GspCommonFieldIncrement> {

    @Override
    public void serialize(GspCommonFieldIncrement value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        writeExtendInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(GspCommonFieldIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.IncrementType, value.getIncrementType().toString());
        switch (value.getIncrementType()) {
            case Added:
                writeAddedIncrement((AddedFieldIncrement) value, gen);
                break;
            case Modify:
                writeModifyIncrement((ModifyFieldIncrement) value, gen);
                break;
            case Deleted:
                writeDeletedIncrement((DeletedFieldIncrement) value, gen);
                break;
            default:
                throw new RuntimeException("增量序列化失败，不存在增量类型" + value.getIncrementType().toString());
        }
    }

    //region add
    private void writeAddedIncrement(AddedFieldIncrement value, JsonGenerator gen) {
        writeBaseAddedInfo(value, gen);
        writeExtendAddedInfo(value, gen);
    }

    private void writeBaseAddedInfo(AddedFieldIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.AddedDataType);
        CefFieldSerializer fieldSerializer = getFieldSerializer();
        fieldSerializer.serialize(value.getAddedField(), gen, null);
    }

    protected abstract CefFieldSerializer getFieldSerializer();

    protected void writeExtendAddedInfo(AddedFieldIncrement value, JsonGenerator gen) {

    }

    //endregion

    //region delete
    private void writeDeletedIncrement(DeletedFieldIncrement value, JsonGenerator gen) {
        writeBaseDeletedInfo(value, gen);
        writeExtendDeletedInfo(value, gen);
    }

    private void writeBaseDeletedInfo(DeletedFieldIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.DeletedId, value.getDeleteId());
    }

    protected void writeExtendDeletedInfo(DeletedFieldIncrement value, JsonGenerator gen) {

    }
    //endregion

    //region modify
    private void writeModifyIncrement(ModifyFieldIncrement value, JsonGenerator gen) {
        writeBaseModifyInfo(value, gen);
        writeExtendModifyInfo(value, gen);
    }

    private void writeBaseModifyInfo(ModifyFieldIncrement value, JsonGenerator gen) {

        HashMap<String, PropertyIncrement> properties = value.getChangeProperties();
        if (properties != null && properties.size() > 0)
            writeProperties(properties, gen);

    }

    private void writeProperties(HashMap<String, PropertyIncrement> properties, JsonGenerator gen) {

        SerializerUtils.writePropertyName(gen, CefNames.PropertyIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : properties.entrySet()) {
            SerializerUtils.writeStartObject(gen);
            SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

            SerializerUtils.writePropertyName(gen, CefNames.Value);
            PropertyIncrementSerializer serializer = getPropertyIncrementSerializer();
            serializer.serialize(item.getValue(), gen, null);
            SerializerUtils.writeEndObject(gen);

        }
        SerializerUtils.WriteEndArray(gen);
    }

    private PropertyIncrementSerializer getPropertyIncrementSerializer() {
        return new PropertyIncrementSerializer();
    }

    protected void writeExtendModifyInfo(ModifyFieldIncrement value, JsonGenerator gen) {

    }
    //endregion

    protected void writeExtendInfo(GspCommonFieldIncrement value, JsonGenerator gen) {

    }
}
