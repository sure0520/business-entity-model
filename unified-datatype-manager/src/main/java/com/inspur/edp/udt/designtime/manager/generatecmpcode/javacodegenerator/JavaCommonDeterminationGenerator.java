/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator;


import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.manager.generatecomponent.JavaCompCodeNames;

public class JavaCommonDeterminationGenerator extends JavaBaseCommonCompCodeGenerator {

  @Override
  protected String getBaseClassName() {
    return JavaCompCodeNames.AbstractDeterminationClassName;
  }

  public JavaCommonDeterminationGenerator(ComplexDataTypeDef cdtDef, CommonOperation operation,
      String nameSpace, String path) {
    super(cdtDef, operation, nameSpace, path);
  }

  @Override
  protected String getNameSpaceSuffix() {
    return JavaCompCodeNames.UDTDeterminationNameSpaceSuffix;
  }

  @Override
  protected void javaGenerateExtendUsing(StringBuilder result) {
    //待定修改成Import

    ///#region using
    result.append(getImportStr(JavaCompCodeNames.DeterminationSpiNameSpace));
    result.append(getImportStr(JavaCompCodeNames.DeterminationApiNameSpace));
    result.append(getImportStr(JavaCompCodeNames.DeterminationChangeset));
    result.append(getImportStr(JavaCompCodeNames.AbstractDeterminationNameSpace));
    result.append(getImportStr(JavaCompCodeNames.IDeterminationContextNameSpace));

    ///#endregion
  }

  @Override
  protected void javaGenerateConstructor(StringBuilder result) {
    result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(getCompName()).append("(IDeterminationContext context, IChangeDetail change)")
        .append(" ").append("{").append(getNewline());

    result.append(getIndentationStr()).append(getIndentationStr()).append("super(context,change)")
        .append(";").append(getNewline());

    result.append(getIndentationStr()).append("}").append(getNewline());
  }

  @Override
  protected void javaGenerateExtendMethod(StringBuilder result) {
    result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
        .append(entityClassName).append(" ").append("getData()").append(" ").append("{")
        .append(getNewline());

    result.append(getIndentationStr()).append(getIndentationStr()).append("return").append(" ")
        .append("(").append(entityClassName).append(")").append("super.getContext().getData()")
        .append(";").append(getNewline());

    result.append(getIndentationStr()).append("}").append(getNewline());
  }

  /**
   * 获取构件名称
   */
  @Override
  protected String getInitializeCompName() {
    return String.format("%1$s%2$s", operation.getCode(), "Determination");
  }
}
