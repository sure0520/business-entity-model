/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager.commonstructure;

import com.inspur.edp.caf.cef.rt.api.CommonStructureInfo;
import com.inspur.edp.caf.cef.rt.spi.EntitySchemaExtension;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.springframework.stereotype.Component;

public class UdtEntitySchemaExtension implements EntitySchemaExtension {
	/**
	 * 获取实体
	 * @param metaId
	 * @return
	 */
	@Override
	public CommonStructure getEntity(String metaId) {
		MetadataRTService service = SpringBeanUtils.getBean(MetadataRTService.class);
		UnifiedDataTypeDef udt = (UnifiedDataTypeDef) service.getMetadata(metaId).getContent();
		return udt;
	}

	@Override
	public CommonStructureInfo getEntitySummary(String s) {
		return null;
	}

	/**
	 * 获取实体类型
	 * @return
	 */
	@Override
	public String getEntityType() {
		return "UnifiedDataType";
	}
}
