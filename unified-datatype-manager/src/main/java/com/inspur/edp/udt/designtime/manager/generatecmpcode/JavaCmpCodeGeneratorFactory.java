/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager.generatecmpcode;

import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.validation.ValidationInfo;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaBaseCommonCompCodeGenerator;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaBaseCompCodeGenerator;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaCommonDeterminationGenerator;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaCommonValidationGenerator;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaValidationGenerator;

;

public final class JavaCmpCodeGeneratorFactory {

  public static JavaBaseCompCodeGenerator javaGetGenerator(UnifiedDataTypeDef udtDef,
      ValidationInfo vldInfo, String nameSpace, String path) {
    return new JavaValidationGenerator(udtDef, vldInfo, nameSpace, path);
  }

  public static JavaBaseCommonCompCodeGenerator javaGetGenerator(UnifiedDataTypeDef udtDef,
      CommonOperation operation, String nameSpace, String path) {
    if (operation instanceof CommonDetermination) {
      return new JavaCommonDeterminationGenerator((ComplexDataTypeDef) udtDef, operation, nameSpace,
          path);
    } else if (operation instanceof CommonValidation) {
      return new JavaCommonValidationGenerator(udtDef, operation, nameSpace, path);
    } else {
      throw new RuntimeException("暂不支持当前操作类型生成构件对java文件！");
    }
  }
}
