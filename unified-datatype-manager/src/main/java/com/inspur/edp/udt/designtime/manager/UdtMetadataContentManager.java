/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager;


import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.MetadataContentManager;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.core.context.ICAFContextService;
import java.util.Date;

public class UdtMetadataContentManager implements MetadataContentManager {

  public final void build(GspMetadata metadata) {
    String metadataID = metadata.getHeader().getId();
    String metadataName = metadata.getHeader().getName();
    String metadataCode = metadata.getHeader().getCode();
    String metadataAssembly = metadata.getHeader().getNameSpace();
    SimpleDataTypeDef sdt = initSimpleDataTypeDef(metadataID, metadataName, metadataCode,
        metadataAssembly);

    metadata.setContent(sdt);
  }

  //注意：Udt元数据初始化在Inspur.Gsp.Udt.WebApi.UdtController中

  /**
   * 初始化单值UDT
   * @param metadataID
   * @param metadataName
   * @param metadataCode
   * @param metadataAssembly
   * @return
   */
  private SimpleDataTypeDef initSimpleDataTypeDef(String metadataID, String metadataName,
      String metadataCode, String metadataAssembly) {
    SimpleDataTypeDef sdt = new SimpleDataTypeDef();
    sdt.setId(metadataID);
    sdt.setCode(metadataCode);
    sdt.setName(metadataName);
    // todo cef runtime 引用
//    sdt.setCreatedDate(Date.from(CAFContext.current.getCurrentDateTime().toInstant()));
//    sdt.setModifiedDate(Date.from(CAFContext.current.getCurrentDateTime().toInstant()));
    // 20190902-wj-恢复删除初始化时AssemblyName属性上的".udt." + udt.Code
    sdt.setDotnetAssemblyName(metadataAssembly + ".udt." + sdt.getCode());
    //sdt.AssemblyName = metadataAssembly;
    return sdt;
  }


}
