/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.i18n;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.i18n.names.UdtResourceDescriptionNames;

public class ComplexUdtReourceExtractor extends BaseUdtResourceExtractor {

    public ComplexUdtReourceExtractor(ComplexDataTypeDef cUdt, ICefResourceExtractContext context) {
        super(cUdt, context, getParentResourcePrefixInfo(cUdt));
    }

    static CefResourcePrefixInfo getParentResourcePrefixInfo(ComplexDataTypeDef cUdt) {
        CefResourcePrefixInfo info = new CefResourcePrefixInfo();
        info.setDescriptionPrefix(UdtResourceDescriptionNames.ComplexUnifiedDataType);
        info.setResourceKeyPrefix(cUdt.getDotnetAssemblyName());
        return info;
    }
}
