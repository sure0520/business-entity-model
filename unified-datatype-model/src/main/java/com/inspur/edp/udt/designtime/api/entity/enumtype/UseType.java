/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.entity.enumtype;

/** 
 属性使用方式
 
*/
public enum UseType
{
	/** 
	 作为约束使用
	 
	*/
	AsConstraint(0),
	/** 
	 作为模板使用
	 
	*/
	AsTemplate(1);

	private int intValue;
	private static java.util.HashMap<Integer, UseType> mappings;
	private synchronized static java.util.HashMap<Integer, UseType> getMappings()
	{
		if (mappings == null)
		{
			mappings = new java.util.HashMap<Integer, UseType>();
		}
		return mappings;
	}

	private UseType(int value)
	{
		intValue = value;
		UseType.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static UseType forValue(int value)
	{
		return getMappings().get(value);
	}
}
