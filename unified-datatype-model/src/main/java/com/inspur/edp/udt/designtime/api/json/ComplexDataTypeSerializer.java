/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.json;


import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmSerializer;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.TransmitType;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.i18n.merger.ComplexUdtResourceMerger;
import com.inspur.edp.udt.designtime.api.utils.UdtThreadLocal;

;

public class ComplexDataTypeSerializer extends UdtSerializer {

    public ComplexDataTypeSerializer(){
    	if(UdtThreadLocal.get()!=null)
    		isFull = UdtThreadLocal.get().getfull();
	}
	public ComplexDataTypeSerializer(boolean full){
		super(full);
    	isFull= full;
	}
	@Override
	protected void writeUdtExtendInfo(JsonGenerator writer, UnifiedDataTypeDef dataType) {
		ComplexDataTypeDef cUdt = (ComplexDataTypeDef) dataType;
        if(isFull||cUdt.getDbInfo()!=null){
        	SerializerUtils.writePropertyName(writer, UdtNames.DbInfo);
        	SerializerUtils.writePropertyValue_Object(writer, cUdt.getDbInfo());
        }
		writeElements(writer, cUdt);
        if(isFull||(cUdt.getTransmitType()!=null&&cUdt.getTransmitType()!= TransmitType.ValueChanged))
        	SerializerUtils.writePropertyValue(writer, UdtNames.TransmitType, cUdt.getTransmitType());
        if(isFull||(cUdt.getDtmBeforeSave()!=null&&cUdt.getDtmBeforeSave().size()>0)){
        	SerializerUtils.writePropertyName(writer, UdtNames.DtmBeforeSave);
        	writeDtms(writer, cUdt.getDtmBeforeSave());
        }
        if(isFull||(cUdt.getDtmAfterCreate()!=null&&cUdt.getDtmAfterCreate().size()>0)){
        	SerializerUtils.writePropertyName(writer, UdtNames.DtmAfterCreate);
        	writeDtms(writer, cUdt.getDtmAfterCreate());
        }
        if(isFull||(cUdt.getDtmAfterModify()!=null&&cUdt.getDtmAfterModify().size()>0)){
        	SerializerUtils.writePropertyName(writer, UdtNames.DtmAfterModify);
        	writeDtms(writer, cUdt.getDtmAfterModify());
        }

	}

	private void writeDtms(JsonGenerator writer, CommonDtmCollection dtms) {
		CommonDtmSerializer serializer = new CommonDtmSerializer(isFull);
		SerializerUtils.writeArray(writer, serializer, dtms);
	}

	private void writeElements(JsonGenerator writer, ComplexDataTypeDef cUdt) {
		if(isFull||(cUdt.getElements()!=null&&cUdt.getElements().size()>0)){
			SerializerUtils.writePropertyName(writer, UdtNames.Elements);
			UdtElementSerializer ser = new UdtElementSerializer(isFull);
			SerializerUtils.writeArray(writer, ser, cUdt.getElements());
		}
	}
}
