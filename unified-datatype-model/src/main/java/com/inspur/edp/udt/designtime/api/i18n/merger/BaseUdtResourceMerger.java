/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.CefFieldResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.merger.DataTypeResourceMerger;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

public class BaseUdtResourceMerger extends DataTypeResourceMerger {

  public BaseUdtResourceMerger(
      UnifiedDataTypeDef commonDataType,
      ICefResourceMergeContext context) {
    super(commonDataType, context);
  }

  @Override
  protected CefFieldResourceMerger getCefFieldResourceMerger(
      ICefResourceMergeContext context, IGspCommonField field) {
    return new UdtEleResourceMerger((UdtElement) field,context);
  }
}
