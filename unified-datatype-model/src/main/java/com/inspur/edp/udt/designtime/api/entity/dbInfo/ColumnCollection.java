/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.entity.dbInfo;

import com.inspur.edp.cef.designtime.api.util.DataValidator;
public class ColumnCollection extends java.util.ArrayList<ColumnInfo>
{
	/** 
	 创建字段集合
	 
	*/
	public ColumnCollection()
	{
	}
	/** 
	 移动字段
	 
	 @param toIndex 目的索引为止
	 @param element 需要移动的字段
	*/
	public final void move(int toIndex, ColumnInfo element)
	{
		if (contains(element))
		{

			super.remove(element);
			//super.insert(toIndex, element);
			super.add(toIndex, element);

		}
	}

	private void innerAdd(ColumnInfo element)

	{
		super.add(element);
	}
	/** 
	 克隆
	 
	// @param absObject
	 //@param parentAssociation
	 @return 
	*/
	public  ColumnCollection clone()
	{
		ColumnCollection newCollection = new ColumnCollection();
		for (ColumnInfo item : this)
		{
			Object tempVar = item.clone();
			newCollection.add((ColumnInfo)((tempVar instanceof ColumnInfo) ? tempVar : null));
		}
		return newCollection;
	}

	/** 
	 Index[string]
	 
	*/
	public final ColumnInfo getItem(String id)
	{
		for(ColumnInfo item:this)
		{
			if(item.getID().equals(id))
			{
				return item;
			}
		}
		return null;
	}

	/** 
	 添加一个新字段
	 
	 @param element
	*/
	@Override
	public final boolean add(ColumnInfo element)
	{
		DataValidator.checkForNullReference(element,"element");
		boolean result=super.add(element);
		return result;
	}
}
