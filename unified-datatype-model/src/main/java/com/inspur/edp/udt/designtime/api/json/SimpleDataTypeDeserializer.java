/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.GspEnumValueDeserializer;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationDeserializer;


public class SimpleDataTypeDeserializer extends UdtDeserializer
{

	@Override
	protected UnifiedDataTypeDef createUnifiedDataType()
	{
		return new SimpleDataTypeDef();
	}
    @Override
	protected void beforeUdtDeserializer(UnifiedDataTypeDef value){
		SimpleDataTypeDef dataType=(SimpleDataTypeDef)value;
		dataType.setLength(0);
		dataType.setPrecision(0);
		dataType.setDefaultValue("");
		dataType.setIsUnique(false);
		dataType.setIsUnique(false);
		dataType.setObjectType(GspElementObjectType.None);
		dataType.setEnableRtrim(false);
		dataType.setChildAssociations(new GspAssociationCollection());
		dataType.setContainEnumValues(new GspEnumValueCollection());
	}
	protected void readBasicInfo(JsonParser jsonParser, UnifiedDataTypeDef value, String propertyName)
	{
		if(!(value instanceof SimpleDataTypeDef )){
			throw new RuntimeException("错误的类型");
		}
		SimpleDataTypeDef dataType=(SimpleDataTypeDef)value;
		switch(propertyName)
		{
			case UdtNames.DataType:
				dataType.setMDataType(SerializerUtils.readPropertyValue_Enum(jsonParser,GspElementDataType.class, GspElementDataType.values()));
				break;
			case UdtNames.Length:
				dataType.setLength(SerializerUtils.readPropertyValue_Integer(jsonParser));
				break;
			case UdtNames.Precision:
				dataType.setPrecision(SerializerUtils.readPropertyValue_Integer(jsonParser));
				break;
			case UdtNames.DefaultValue:
				dataType.setDefaultValue(SerializerUtils.readPropertyValue_String(jsonParser,null));
				break;
			case UdtNames.IsUnique:
				dataType.setIsUnique(SerializerUtils.readPropertyValue_boolean(jsonParser));
				break;
			case UdtNames.IsRequired:
				dataType.setIsRequired(SerializerUtils.readPropertyValue_boolean(jsonParser));
				break;
			case UdtNames.EnableRtrim:
				dataType.setEnableRtrim(SerializerUtils.readPropertyValue_boolean(jsonParser));
				break;
			case UdtNames.ObjectType:
				dataType.setObjectType(SerializerUtils.readPropertyValue_Enum(jsonParser,GspElementObjectType.class, GspElementObjectType.values()));
				break;
			case UdtNames.ContainEnumValues:
				readEnumValueList(jsonParser, dataType);
				break;
			case UdtNames.ChildAssociations:
				readAssociationCollection(jsonParser, dataType);
				break;
            case UdtNames.EnumIndexType:
                dataType.setEnumIndexType(SerializerUtils.readPropertyValue_Enum(jsonParser,EnumIndexType.class, EnumIndexType.values()));
                break;
			default:
				throw new RuntimeException("未定义的属性名" + propertyName);

		}

	}


	public static void readEnumValueList(JsonParser jsonParser, SimpleDataTypeDef dataType)
	{
		GspEnumValueCollection collection =new GspEnumValueCollection();
		GspEnumValueDeserializer der=new GspEnumValueDeserializer();
		SerializerUtils.readArray(jsonParser,der,collection);
		dataType.setContainEnumValues(collection);
	}

	private void readAssociationCollection(JsonParser jsonParser, SimpleDataTypeDef dataType) {
//		GspAssociationDeserializer converter = new GspAssociationDeserializer(new UdtElementDeserializer());
		UdtAssociationDeserializer converter=new UdtAssociationDeserializer(new UdtElementDeserializer());
		GspAssociationCollection collection = new GspAssociationCollection();
		SerializerUtils.readArray(jsonParser, converter, collection);
		dataType.setChildAssociations(collection);
	}
}
