/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.entity.property;

import com.inspur.edp.cef.designtime.api.collection.BaseList;

/**
 属性集合
 
*/
public class PropertyCollection extends BaseList<PropertyInfo> implements Cloneable
{
	public final boolean add(PropertyInfo info)
	{
		if (contains(info))
		{
			throw new RuntimeException("已存在名称为【" + info.getPropertyName() + "】的属性。");
		}
		return super.add(info);
	}

	/** 
	 获取关联外键
	 
	*/
	public PropertyInfo getIndex(int index)

	{
		return (PropertyInfo) ((this.get(index) instanceof PropertyInfo) ? this.get(index) : null);
	}
	public PropertyInfo getPropertyName(String propertyName)

	{
	   return 	 getPropertyInfo(propertyName);
	}

	private PropertyInfo getPropertyInfo(String name)
	{
		for (PropertyInfo propertyInfo : this)
		{
			if (propertyInfo == null)
			{
				continue;
			}
			if (propertyInfo.getPropertyName().equals(name))
			{
				return propertyInfo;
			}

		}

		throw new RuntimeException("不存在名称为【" + name + "】的属性");
	}

	public final boolean contains(PropertyInfo info)
	{
		for (PropertyInfo propertyInfo : this)
		{
			if (propertyInfo == null)
			{
				continue;
			}
			if (propertyInfo.getPropertyName().equals(info.getPropertyName()))
			{
				return true;
			}
		}
		return false;
	}

	public final boolean contains(String propertyName)
	{
		for (PropertyInfo item : this)
		{
			if (item.getPropertyName().equals(propertyName))
			{
				return true;
			}
		}
		return false;
	}

	public final PropertyCollection clone()
	{
		PropertyCollection newCollection = new PropertyCollection();
		for (PropertyInfo propertyInfo : this)
		{
			newCollection.add(propertyInfo.clone());
		}

		return newCollection;
	}
}
