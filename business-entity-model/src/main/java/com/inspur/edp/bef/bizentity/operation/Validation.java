/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.beenum.BEValidationType;
import com.inspur.edp.bef.bizentity.beenum.RequestNodeTriggerType;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;
import java.util.EnumSet;

/**
 * 校验规则
 */
public class Validation extends BizOperation implements Cloneable {

  /**
   * 默认构造方法
   */
  public Validation() {
  }


  public BizParameterCollection getParameterCollection() {
    return parameterCollection;
  }

  public void setParameterCollection(BizParameterCollection parameterCollection) {
    this.parameterCollection = parameterCollection;
  }

  private BizParameterCollection parameterCollection;
  /**
   * 类型：操作许可校验、数据一致性校验。
   */
  private BEValidationType privateValidationType = BEValidationType.forValue(0);

  public BEValidationType getValidationType() {
    return privateValidationType;
  }

  public void setValidationType(BEValidationType value) {
    privateValidationType = value;
  }

  /**
   * 执行时机
   */
  private EnumSet<BETriggerTimePointType> privateTriggerTimePointType = EnumSet
      .of(BETriggerTimePointType.forValue(0));

  public final EnumSet<BETriggerTimePointType> getTriggerTimePointType() {
    return privateTriggerTimePointType;
  }

  public final void setTriggerTimePointType(EnumSet<BETriggerTimePointType> value) {
    privateTriggerTimePointType = value;
  }

  /**
   * 执行时机
   */
  private EnumSet<RequestNodeTriggerType> privateRequestNodeTriggerType = EnumSet
      .of(RequestNodeTriggerType.forValue(0));

  public EnumSet<RequestNodeTriggerType> getRequestNodeTriggerType() {
    return privateRequestNodeTriggerType;
  }

  public void setRequestNodeTriggerType(EnumSet<RequestNodeTriggerType> value) {
    privateRequestNodeTriggerType = value;
  }

  private ValElementCollection rqtElements;

  public final void setRequestElements(ValElementCollection value) {
    rqtElements = value;
  }

  public final ValElementCollection getRequestElements() {
    if (rqtElements == null) {
      rqtElements = new ValElementCollection();
    }
    return rqtElements;
  }

  private java.util.HashMap<String, ValElementCollection> requestChildElements;

  public final void setRequestChildElements(java.util.HashMap<String, ValElementCollection> value) {
    requestChildElements = value;
  }

  public final java.util.HashMap<String, ValElementCollection> getRequestChildElements() {
    if (requestChildElements == null) {
      requestChildElements = new java.util.HashMap<String, ValElementCollection>();
    }
    return requestChildElements;
  }

  private java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> validationTriggerPoints;

  public final void setValidationTriggerPoints(
      java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> value) {
    validationTriggerPoints = value;
  }

  public final java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> getValidationTriggerPoints() {
    if (validationTriggerPoints == null) {
      validationTriggerPoints = new java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>>();
    }
    return validationTriggerPoints;
  }


  @Override
  public boolean equals(Object obj) {
    Validation other = (Validation) ((obj instanceof Validation) ? obj : null);
    return other != null && ((this == other) || equals(other));
  }

  protected final boolean equals(Validation other) {
    return super.equals(other) && other.getValidationType() == getValidationType()
        && other.getRequestNodeTriggerType() == getRequestNodeTriggerType();
  }

  @Override
  public int hashCode() {
    int hashCode = (super.hashCode() * 397);
    hashCode = (hashCode * 397) ^ getValidationType().hashCode();
    hashCode = (hashCode * 397) ^ getRequestNodeTriggerType().hashCode();
    return hashCode;
  }

  @Override
  public Validation clone() {
    return clone(false);
  }

  @Override
  public Validation clone(boolean isGenerateId) {
    Object tempVar = super.clone(isGenerateId);
    Validation result = (Validation) ((tempVar instanceof Validation) ? tempVar : null);
    return result;
  }

  public final void mergeWithDependentValidation(Validation dependentValidation) {
    if (getIsRef() || dependentValidation == null) {
      return;
    }
    mergeOperationBaseInfo(dependentValidation);
    mergeValidationInfo(dependentValidation);
  }

  private void mergeValidationInfo(Validation dependentValidation) {
    setValidationType(dependentValidation.getValidationType());
    setRequestNodeTriggerType(dependentValidation.getRequestNodeTriggerType());
  }

  @Override
  public BEOperationType getOpType() {
    return BEOperationType.Validation;
  }
}
