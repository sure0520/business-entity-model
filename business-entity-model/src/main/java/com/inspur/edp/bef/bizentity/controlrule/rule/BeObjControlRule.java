/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.controlrule.rule;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.rule.parser.BeObjectControlRuleParser;
import com.inspur.edp.bef.bizentity.controlrule.rule.serializer.BeObjectControlRuleSerializer;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeEntityRuleDefNames;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;

@JsonSerialize(using = BeObjectControlRuleSerializer.class)
@JsonDeserialize(using = BeObjectControlRuleParser.class)
public class BeObjControlRule extends CmEntityControlRule {

  public ControlRuleItem getAddUQConstraintControlRule() {
    return super.getControlRule(BeEntityRuleDefNames.AddUQConstraint);
  }

  public void setAddUQConstraintControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeEntityRuleDefNames.AddUQConstraint, ruleItem);
  }

  public ControlRuleItem getModifyUQConstraintControlRule() {
    return super.getControlRule(BeEntityRuleDefNames.ModifyUQConstraint);
  }

  public void setModifyUQConstraintControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeEntityRuleDefNames.ModifyUQConstraint, ruleItem);
  }

  public ControlRuleItem getAddDeterminationControlRule() {
    return super.getControlRule(BeEntityRuleDefNames.AddDtermination);
  }

  public void setAddDterminationControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeEntityRuleDefNames.AddDtermination, ruleItem);
  }

  public ControlRuleItem getModifyDterminationControlRule() {
    return super.getControlRule(BeEntityRuleDefNames.ModifyDetermination);
  }

  public void setModifyDterminationControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeEntityRuleDefNames.ModifyDetermination, ruleItem);
  }

  public ControlRuleItem getAddValidationControlRule() {
    return super.getControlRule(BeEntityRuleDefNames.AddValidation);
  }

  public void setAddValidationControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeEntityRuleDefNames.AddValidation, ruleItem);
  }

  public ControlRuleItem getModifyValidationControlRule() {
    return super.getControlRule(BeEntityRuleDefNames.ModifyValidation);
  }

  public void setModifyValidationControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeEntityRuleDefNames.ModifyValidation, ruleItem);
  }
}
