/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.pushchangesetargs;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import java.util.Map;

public class ObjectChangeDetail {

  private String bizObjectId;
  private String bizObjectCode;
  private Boolean isNecessary;
  private GspBizEntityObject bizObject;
  private Map<String, Object> changeInfo;
  private String parentObjIDElementId;

  public String getParentObjIDElementId() {
    return parentObjIDElementId;
  }

  public void setParentObjIDElementId(String parentObjIDElementId) {
    this.parentObjIDElementId = parentObjIDElementId;
  }


  public ObjectChangeDetail() {
  }

  public String getBizObjectId() {
    return bizObjectId;
  }

  public void setBizObjectId(String bizObjectId) {
    this.bizObjectId = bizObjectId;
  }

  public String getBizObjectCode() {
    return bizObjectCode;
  }

  public void setBizObjectCode(String bizObjectCode) {
    this.bizObjectCode = bizObjectCode;
  }

  public Boolean getNecessary() {
    return isNecessary;
  }

  public void setNecessary(Boolean necessary) {
    isNecessary = necessary;
  }

  public GspBizEntityObject getBizObject() {
    return bizObject;
  }

  public void setBizObject(GspBizEntityObject bizObject) {
    this.bizObject = bizObject;
  }

  public Map<String, Object> getChangeInfo() {
    return changeInfo;
  }

  public void setChangeInfo(Map<String, Object> changeInfo) {
    this.changeInfo = changeInfo;
  }
}
