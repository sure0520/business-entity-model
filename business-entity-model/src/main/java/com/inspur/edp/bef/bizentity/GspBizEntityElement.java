/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.beenum.RequiredCheckOccasion;
import com.inspur.edp.bef.bizentity.commonstructure.BeCommonStructureUtil;
import com.inspur.edp.bef.bizentity.json.element.BizElementDeserializer;
import com.inspur.edp.bef.bizentity.json.element.BizElementSerializer;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.collection.CommonValCollection;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.entity.commonstructure.CefCommonStructureUtil;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.das.commonmodel.entity.element.ElementCodeRuleConfig;
import com.inspur.edp.das.commonmodel.entity.element.GspCommonAssociation;
import lombok.Getter;
import lombok.Setter;


/**
 * BE字段实体类
 */
@JsonDeserialize(using = BizElementDeserializer.class)
@JsonSerialize(using = BizElementSerializer.class)
public class GspBizEntityElement extends GspCommonElement implements Cloneable {

  private transient RequiredCheckOccasion requiredCheckOccasion = RequiredCheckOccasion.All;

  // region 属性
  /**
   * 是否默认为空值
   */
  private boolean isDefaultNull = false;

  public boolean getIsDefaultNull() {
    return isDefaultNull;
  }

  public void setIsDefaultNull(boolean value) {
    isDefaultNull = value;
  }

  /**
   * 计算表达式
   * <p>
   * 不采用封装类的原因是有可能会在代码中修改，如果记录Element，依赖的Element用户就不好处理了
   */
  private transient String privateCalculationExpress;

  public final String getCalculationExpress() {
    return privateCalculationExpress;
  }

  public final void setCalculationExpress(String value) {
    privateCalculationExpress = value;
  }

  /**
   * 验证表达式
   */
  private transient String privateValidationExpress;

  public final String getValidationExpress() {
    return privateValidationExpress;
  }

  public final void setValidationExpress(String value) {
    privateValidationExpress = value;
  }

  private String rtElementConfigId;

  public String getRtElementConfigId() {
    return rtElementConfigId;
  }

  public void setRtElementConfigId(String rtElementConfigId) {
    this.rtElementConfigId = rtElementConfigId;
  }

  /**
   * 必填检查时机
   */
  public final RequiredCheckOccasion getRequiredCheckOccasion() {
    return requiredCheckOccasion;
  }

  public final void setRequiredCheckOccasion(RequiredCheckOccasion value) {
    requiredCheckOccasion = value;
  }

  @Getter
  @Setter
  /** 值变化后事件 */
  private CommonDtmCollection valueChangedDtms;

  @Getter
  @Setter
  private CommonDtmCollection computationDtms;

  @Getter
  @Setter
  /** 值变化后校验事件 */
  private CommonValCollection valueChangedVals;
  // endregion

  // region 方法

  public final void mergeWithDependentElement(GspBizEntityElement dependentElement) {
    if (getIsRef()) {
      mergeElementBaseInfo(dependentElement);
    }
    mergeChildAssociations(dependentElement, getIsRef());
  }

  private void mergeElementBaseInfo(GspBizEntityElement dependentElement) {
    setCode(dependentElement.getCode());
    setName(dependentElement.getName());
    setLabelID(dependentElement.getLabelID());
    setMDataType(dependentElement.getMDataType());
    setDefaultValue(dependentElement.getDefaultValue());
    setDefaultValueType(dependentElement.getDefaultValueType());
    setLength(dependentElement.getLength());
    setPrecision(dependentElement.getPrecision());
    setObjectType(dependentElement.getObjectType());
    // ColumnID = dependentElement.ColumnID;
    setIsVirtual(dependentElement.getIsVirtual());
    setIsRequire(dependentElement.getIsRequire());
    setIsCustomItem(dependentElement.getIsCustomItem());
    setIsRefElement(dependentElement.getIsRefElement());
    setRefElementId(dependentElement.getRefElementId());
    setIsMultiLanguage(dependentElement.getIsMultiLanguage());
    setBelongModelID(dependentElement.getBelongModelID());

    setContainEnumValues(dependentElement.getContainEnumValues());
    mergeBillCode(dependentElement.getBillCodeConfig());

    setReadonly(dependentElement.getReadonly());
    setRequiredCheckOccasion(dependentElement.getRequiredCheckOccasion());
    setCalculationExpress(dependentElement.getCalculationExpress());
    setValidationExpress(dependentElement.getValidationExpress());

  }

  private void mergeBillCode(ElementCodeRuleConfig dependentConfig) {
    if (dependentConfig == null) {
      return;
    }
    super.setBillCodeConfig(dependentConfig.clone());
  }

  private void mergeChildAssociations(GspBizEntityElement dependentElement, boolean isRef) {
    if (getChildAssociations() == null || getChildAssociations().size() < 1) {
      return;
    }
    for (GspAssociation childAssociation : getChildAssociations()) {
      GspCommonAssociation dependentAssociation = (GspCommonAssociation) (dependentElement
          .getChildAssociations()
          .getItem(childAssociation.getId()));
      mergeChildAssociation((GspCommonAssociation) childAssociation, dependentAssociation, isRef);
    }
  }

  private void mergeChildAssociation(GspCommonAssociation childAssociation,
      GspCommonAssociation dependentAssociation,
      boolean isRef) {
    if (isRef) {
      mergeAssociationBaseInfo(childAssociation, dependentAssociation);
    }
    mergeRefElements(childAssociation, dependentAssociation);
  }

  private void mergeAssociationBaseInfo(GspCommonAssociation childAssociation,
      GspCommonAssociation dependentAssociation) {
    childAssociation.setRefModelID(dependentAssociation.getRefModelID());
    childAssociation.setRefModelCode(dependentAssociation.getRefModelCode());
    childAssociation.setRefModelName(dependentAssociation.getRefModelName());
    childAssociation.setWhere(dependentAssociation.getWhere());
    childAssociation.setAssSendMessage(dependentAssociation.getAssSendMessage());
    childAssociation
        .setForeignKeyConstraintType(dependentAssociation.getForeignKeyConstraintType());
    childAssociation.setDeleteRuleType(dependentAssociation.getDeleteRuleType());
    for (com.inspur.edp.cef.designtime.api.element.GspAssociationKey GspAssociationKey : dependentAssociation
        .getKeyCollection()) {
      childAssociation.getKeyCollection().addAssociation(GspAssociationKey);
    }

  }

  private void mergeRefElements(GspCommonAssociation childAssociation,
      GspCommonAssociation dependentAssociation) {
    if (childAssociation.getRefElementCollection() == null
        || childAssociation.getRefElementCollection().size() < 1) {
      return;
    }
    for (IGspCommonField refElement : childAssociation.getRefElementCollection()) {
      GspBizEntityElement dependentRefElement = (GspBizEntityElement) dependentAssociation
          .getRefElementCollection()
          .getItem(refElement.getID());
      ((GspBizEntityElement) refElement).mergeWithDependentElement(dependentRefElement);
    }
  }
  //endregion

  // region Property
  @Override
  protected CefCommonStructureUtil getCefCommonStructureUtil() {
    return BeCommonStructureUtil.getInstance();
  }

  // endregion
}
