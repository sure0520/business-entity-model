/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser.BeEntityControlRuleDefParser;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.serializer.BeEntityControlRuleDefSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

@JsonSerialize(using = BeEntityControlRuleDefSerializer.class)
@JsonDeserialize(using = BeEntityControlRuleDefParser.class)
public class BeEntityControlRuleDef extends CmEntityControlRuleDef {

  public BeEntityControlRuleDef(BeControlRuleDef parentRuleDefinition) {
    super(parentRuleDefinition);
    super.setRuleObjectType(BeEntityRuleDefNames.BeEntityRuleObjectType);
    init();
  }


  private void init() {

    ControlRuleDefItem nameRule = new ControlRuleDefItem() {
      {
        this.setRuleName(CmRuleNames.Name);
        this.setRuleDisplayName("名称");
        this.setDescription("节点名称");
        this.setDefaultRuleValue(ControlRuleValue.Allow);
      }
    };
    setNameControlRule(nameRule);

    ControlRuleDefItem addUqRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeEntityRuleDefNames.AddUQConstraint);
        this.setRuleDisplayName("添加唯一性约束");
        this.setDescription("添加唯一性约束");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setAddUQConstraintControlRule(addUqRule);

    ControlRuleDefItem modifyUqRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeEntityRuleDefNames.ModifyUQConstraint);
        this.setRuleDisplayName("修改唯一性约束");
        this.setDescription("修改唯一性约束");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setModifyUQConstraintControlRule(modifyUqRule);

    ControlRuleDefItem addDtmRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeEntityRuleDefNames.AddDtermination);
        this.setRuleDisplayName("添加联动计算");
        this.setDescription("添加联动计算");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setAddDterminationControlRule(addDtmRule);

    ControlRuleDefItem modifyDtmRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeEntityRuleDefNames.ModifyDetermination);
        this.setRuleDisplayName("修改联动计算");
        this.setDescription("修改联动计算");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setModifyDterminationControlRule(modifyDtmRule);

    ControlRuleDefItem addValRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeEntityRuleDefNames.AddValidation);
        this.setRuleDisplayName("添加校验规则");
        this.setDescription("添加校验规则");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setAddValidationControlRule(addValRule);

    ControlRuleDefItem modifyValRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeEntityRuleDefNames.ModifyValidation);
        this.setRuleDisplayName("修改校验规则");
        this.setDescription("修改校验规则");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setModifyValidationControlRule(modifyValRule);

    ControlRuleDefItem addChildEntityRule = new ControlRuleDefItem() {
      {
        this.setRuleName(CmEntityRuleNames.AddChildEntity);
        this.setRuleDisplayName("添加子节点");
        this.setDescription("添加子节点");
        this.setDefaultRuleValue(ControlRuleValue.Allow);
      }
    };
    setAddChildEntityControlRule(addChildEntityRule);

    ControlRuleDefItem modifyChildEntityRule = new ControlRuleDefItem() {
      {
        this.setRuleName(CmEntityRuleNames.ModifyChildEntities);
        this.setRuleDisplayName("修改子节点信息");
        this.setDescription("修改子节点信息");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setModifyChildEntitiesControlRule(modifyChildEntityRule);

    ControlRuleDefItem addElementRule = new ControlRuleDefItem() {
      {
        this.setRuleName(CommonDataTypeRuleNames.AddField);
        this.setRuleDisplayName("添加字段");
        this.setDescription("添加字段");
        this.setDefaultRuleValue(ControlRuleValue.Allow);
      }
    };
    setAddFieldControlRule(addElementRule);

    ControlRuleDefItem modifyElementRule = new ControlRuleDefItem() {
      {
        this.setRuleName(CommonDataTypeRuleNames.ModifyFields);
        this.setRuleDisplayName("修改字段信息");
        this.setDescription("修改字段信息");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setModifyFieldsControlRule(modifyElementRule);

//        BeEntityControlRuleDef objectRuleDef = new BeEntityControlRuleDef(this);
//        getChildControlRules().put(CommonModelNames.MainObject, objectRuleDef);
    BeFieldControlRuleDef fieldRuleDef = new BeFieldControlRuleDef(this);
    getChildControlRules().put(CommonModelNames.Element, fieldRuleDef);
  }

  public ControlRuleDefItem getAddUQConstraintControlRule() {
    return super.getControlRuleItem(BeEntityRuleDefNames.AddUQConstraint);
  }

  public void setAddUQConstraintControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeEntityRuleDefNames.AddUQConstraint, ruleItem);
  }

  public ControlRuleDefItem getModifyUQConstraintControlRule() {
    return super.getControlRuleItem(BeEntityRuleDefNames.ModifyUQConstraint);
  }

  public void setModifyUQConstraintControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeEntityRuleDefNames.ModifyUQConstraint, ruleItem);
  }

  public ControlRuleDefItem getAddDeterminationControlRule() {
    return super.getControlRuleItem(BeEntityRuleDefNames.AddDtermination);
  }

  public void setAddDterminationControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeEntityRuleDefNames.AddDtermination, ruleItem);
  }

  public ControlRuleDefItem getModifyDterminationControlRule() {
    return super.getControlRuleItem(BeEntityRuleDefNames.ModifyDetermination);
  }

  public void setModifyDterminationControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeEntityRuleDefNames.ModifyDetermination, ruleItem);
  }

  public ControlRuleDefItem getAddValidationControlRule() {
    return super.getControlRuleItem(BeEntityRuleDefNames.AddValidation);
  }

  public void setAddValidationControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeEntityRuleDefNames.AddValidation, ruleItem);
  }

  public ControlRuleDefItem getModifyValidationControlRule() {
    return super.getControlRuleItem(BeEntityRuleDefNames.ModifyValidation);
  }

  public void setModifyValidationControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeEntityRuleDefNames.ModifyValidation, ruleItem);
  }
}
