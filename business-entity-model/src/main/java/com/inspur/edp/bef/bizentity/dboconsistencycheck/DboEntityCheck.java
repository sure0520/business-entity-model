/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.dboconsistencycheck;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import org.springframework.util.StringUtils;

/**
 * DBO实体检查
 *
 * @author haoxiaofei
 */
public class DboEntityCheck {

  private final GspBusinessEntity bizEntity;
  private final StringBuffer expMessage;

  public DboEntityCheck(GspBusinessEntity bizEntity) {
    this.bizEntity = bizEntity;
    this.expMessage = new StringBuffer();
  }

  public String checkEntityCoincidence() {
    if (this.bizEntity.getIsVirtual()) {
      return null;
    }

    checkObjectCoincidence(bizEntity.getMainObject());
    for (IGspCommonObject obj : bizEntity.getMainObject().getContainChildObjects()) {
      checkObjectCoincidence((GspBizEntityObject) obj);
    }
    return this.expMessage.toString();
  }

  private void checkObjectCoincidence(GspBizEntityObject obj) {
    DboObjectCheck checker = new DboObjectCheck(bizEntity, obj);
    String returnStr = checker.checkObjCoincidence();
    if (!StringUtils.isEmpty(returnStr)) {
      this.expMessage.append(returnStr);
    }
  }

}
