/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.operation.beomponent.BizActionParamCollection;
// import IBizParameterCollection;

/**
 * BE基本操作
 */
public class BizAction extends BizActionBase implements Cloneable {

  private BizActionParamCollection beParams;

  public BizAction() {
    super();
    beParams = new BizActionParamCollection();
  }

  /**
   * 操作类型
   */
  @Override
  public BEOperationType getOpType() {
    return BEOperationType.BizAction;
  }

  @Override
  protected BizActionParamCollection getActionParameters() {
    return beParams;
  }
}
