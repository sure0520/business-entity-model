/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.core.utilsgenerator;

/**
 * 参数信息
 */
public class ParameterInfo {
    /**
     * 参数名称
     */
    private String paramName;
    /**
     * 参数类型
     */
    private TypeRefInfo paramType;

    public ParameterInfo(String paramName, TypeRefInfo paramType) {
        this.paramName = paramName;
        this.paramType = paramType;
    }

    public TypeRefInfo getParamType() {
        return paramType;
    }

    public String getParamName() {
        return paramName;
    }

    public void write(StringBuilder stringBuilder) {
        paramType.write(stringBuilder);
        stringBuilder.append(" ");
        stringBuilder.append(getParamName());
    }
}
