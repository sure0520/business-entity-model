/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;

public abstract class DataTypeUtilsGenerator  {
    private IGspCommonDataType commonDataType;

    private JavaClassInfo classInfo=new JavaClassInfo();


    public DataTypeUtilsGenerator(IGspCommonDataType commonDataType,String basePath) {
        this.commonDataType = commonDataType;
        classInfo.setFilePath(basePath);
    }

    protected abstract  String getClassName();
    protected abstract String getClassPackage();

    public  final void generate()
    {
        classInfo.setPackageName(getClassPackage());
        classInfo.setClassName(getClassName());
        classInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        classInfo.getAccessModifiers().add(JavaAccessModifier.Final);

        generateFieldsMethods( );
        generateExtendInfos();
        classInfo.write2File();
    }

    protected void generateExtendInfos( )
    {}

    private void generateFieldsMethods( )
    {
        for (IGspCommonField field:commonDataType.getContainElements())
        {
            generateFieldMethods(field);
        }
    }

    private void generateFieldMethods(IGspCommonField field) {
        generateFieldGetMethod(field);
        generateFieldSetMethod(field);
    }

    private void generateFieldGetMethod(IGspCommonField field) {
        new FieldGetMethodGenerator(field,classInfo).generate();
    }

    private void generateFieldSetMethod(IGspCommonField field) {
        new FieldSetMethodGenerator(field,classInfo).generate();
    }

    protected JavaClassInfo getClassInfo() {
        return classInfo;
    }
}
