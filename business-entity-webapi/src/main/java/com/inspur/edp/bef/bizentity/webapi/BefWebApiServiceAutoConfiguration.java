
package com.inspur.edp.bef.bizentity.webapi;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BefWebApiServiceAutoConfiguration {
	@Bean
	public BeCodeGenController testBefWebApi() {
		return new BeCodeGenController();
	}

	@Bean
	public RESTEndpoint getBefWebApiEndpoint(BeCodeGenController generateService) {
		return new RESTEndpoint(
				"/dev/main/v1.0/bef",
				generateService
		);
	}
}

